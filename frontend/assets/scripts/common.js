import $ from 'jquery';
import 'slick-carousel';

$(document).ready(() => {
  // スライダー
  $('.js-top-slider').slick({
    speed: 1000,
    autoplay: true,
    autoplaySpeed: 3000,
    fade: true,
    arrows: false,
  });

  $('#season-slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '#season-slider-nav'
  });
  $('#season-slider-nav').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '#season-slider',
    focusOnSelect: true,
    arrows: true,
    prevArrow:'<div class="d-season-photoNavIcon is-prev"></div>',
    nextArrow:'<div class="d-season-photoNavIcon is-next"></div>',
  });

  // SPナビ
  $('.js-headerSp-open').on('click', function() {
    $('.c-headerSp').addClass('is-open');
    $('.c-navSp').fadeIn();
  });

  $('.js-headerSp-close').on('click', function() {
    $('.c-headerSp').removeClass('is-open');
    $('.c-navSp').fadeOut();
  });

  $(window).scroll(function() {
    var scrollY = $(window).scrollTop();
    var windowWidth = $(window).width();
    if (scrollY > 200 && windowWidth < 768) {
      $('.js-headerSp').slideDown();
    } else {
      $('.js-headerSp').slideUp();
    }
  });

  // #で始まるリンクをクリックしたら実行する
  // スムーズにスクロールする
  $('a[href^="#"]').click(function(){
    var speed = 1000; // スクロールの速度
    var href= $(this).attr("href");
    var target = $(href == "#" || href == "" ? 'html' : href);
    var position = target.offset().top;
    $("html, body").animate({scrollTop:position}, speed, "swing");
    return false;
  });


  // アニメーション
  $(window).scroll(function() {
    $('.fade').each(function() {
      var scroll = $(window).scrollTop();
      var thisPosition = $(this).offset().top;
      var windowHeight = $(window).height();
      if (scroll > thisPosition - windowHeight + 200) {
        $(this).addClass('fadeIn');
      }
    });
  });
});
