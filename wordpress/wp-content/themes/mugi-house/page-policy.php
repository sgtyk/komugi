<?php get_header(); ?>

<div class="c-page-content">
  <div class="c-page-header" style="background-image: url('/komugi/assets/images/main_policy.jpg');">
    <h1 class="c-page-headerText">大事にしていること</h1>
  </div>
  <ul class="c-page-breadList">
    <li class="c-page-bredItem">
      <a href="/komugi/">トップ</a>
    </li>
    <li class="c-page-bredItem">園のこと - 大事にしていること</li>
  </ul>

  <div class="d-policy-copy">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/policy/policy_text01.png" alt="自分で成長しようとする力を信じ、強い心を育てたい。" class="d-policy-copyTitle u-sp-none">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/policy/policy_text03.png" alt="自分で成長しようとする力を信じ、強い心を育てたい。" class="d-policy-copyTitle u-pc-none">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/policy/policy_text02.png" alt="生きることの意味を自分でみつけられる心を。自分の五感を使って、正しいものを選び取る力を。天に向かって伸びる麦の穂のような、凛とした強さを。" class="d-policy-copyText">
  </div>

  <div class="d-policy-content">
    <div class="d-policy-comment">
      <div class="d-policy-commentTextWrap">
        <p class="d-policy-commentText">
          子どもたち一人ひとりの持っている個性を大切にしながら、自分で成長しようとする力を尊重し、見守る保育でありたいと考えています。
        </p>
        <p class="d-policy-commentText">
          力を蓄えている時期は急かさずにそっと待ち、伸びていく時にこそ支えてあげたい。そのために、子どもたちが自分で選び、自由に想像して遊びや創作を楽しめるような、豊かな環境をつくるよう心がけています。
        </p>
      </div>
      <div class="d-policy-commentPhoto">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/policy/policy_img01.png" alt="長岡 好 園長" class="d-policy-commentImg fade">
        <p class="d-policy-commentName">園長 長岡 好</p>
      </div>
    </div>
    <div class="d-policy-photoList">
      <div class="d-policy-photoItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/policy/policy_img02.png" alt="" class="d-policy-photoImg fade">
        <div class="d-policy-photoTextWrap">
          <p class="d-policy-photoText">毎日の生活では、子どもたちが本物に出会い、<br>五感を使って体験できるような時間を大切にしています。</p>
          <p class="d-policy-photoText">CD の音源ではなく楽器の生演奏を、<br>TV 画面ではなく実際の演劇鑑賞を。</p>
          <p class="d-policy-photoText">機械を通さない、人と人との触れ合いから生まれる<br class="u-sp-none">表現や遊びを大切にしています。</p>
        </div>
      </div>

      <div class="d-policy-photoItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/policy/policy_img03.png" alt="" class="d-policy-photoImg fade">
        <div class="d-policy-photoTextWrap">
          <p class="d-policy-photoText">四季の移り変わりや自然の豊かさも肌で感じられるように、<br class="u-sp-none">野菜や植物を育てながら土に触れたり、水の中で身体を動かしたり、日々の遊びや季節の行事を通して、人間性や創造性を育んでいきます。</p>
          <p class="d-policy-photoText">また、地域の方へのあいさつや交流活動なども積極的に行い、<br class="u-sp-none">地域とともに育っていく園を目指しています。</p>
        </div>
      </div>

      <div class="d-policy-photoItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/policy/policy_img04.png" alt="" class="d-policy-photoImg fade">
        <div class="d-policy-photoTextWrap">
          <p class="d-policy-photoText">創立当初から一番大切にしていることは、<br>「へこたれない子どもをつくる」ということ。</p>
          <p class="d-policy-photoText">こむぎ保育園では、季節を問わず、毎日散歩に出かけてたくさん歩きます。<br>健康で丈夫な身体をつくることは、強い心を育てることにつながります。<br>天に向かって伸びる麦の穂のような、たくましさと健やかさを、<br class="u-sp-none">人生の基礎となる幼児期に身につけてほしい。<br>それが、こむぎ保育園の願いです。</p>
        </div>
      </div>
    </div>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/policy/policy_bg02.png" alt="" class="d-policy-bgBottom u-sp-none">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/policy/policy_bg04.png" alt="" class="d-policy-bgBottom u-pc-none">
  </div><!-- /.d-policy-content -->

  <div class="d-policy-link">
    <p class="d-policy-linkText u-sp-none">
      こむぎ保育園は、さまざまなハンディキャップのある<br>子どもたちを預かる「小金井育児園」から始まりました。<br>詳しい歴史は友好福祉会のページへ→
    </p>
    <p class="d-policy-linkText u-pc-none">
      こむぎ保育園は、障害のあるなしに関わらず<br>子どもを預かる「小金井育児園」から始まりました。<br>詳しい歴史は友好福祉会のページへ↓
    </p>
    <a href="http://komugi.info/" target="_blank">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/policy/policy_banner.jpg" alt="" class="d-policy-linkImg">
    </a>
  </div>
</div><!-- /.c-page-content -->

<?php get_footer(); ?>
