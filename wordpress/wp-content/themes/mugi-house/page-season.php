<?php get_header(); ?>

<div class="c-page-content">
  <div class="c-page-header" style="background-image: url('/komugi/assets/images/main_season.jpg');">
    <h1 class="c-page-headerText">季節のたのしみ</h1>
  </div>
  <ul class="c-page-breadList">
    <li class="c-page-bredItem">
      <a href="/komugi/">トップ</a>
    </li>
    <li class="c-page-bredItem">園のこと - 季節のたのしみ</li>
  </ul>

  <div class="d-season-copy">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/season/season_text01.png" alt="季節に合わせた行事などを通して、日本の伝統や四季を感じる心を育てます。" class="d-season-copyTitle u-sp-none">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/season/season_text03.png" alt="季節に合わせた行事などを通して、日本の伝統や四季を感じる心を育てます。" class="d-season-copyTitle u-pc-none">
    <p class="d-season-copyText">こむぎ保育園では、季節の行事や地域との交流など、暮らしをたのしみ日常に感謝する気持ちを持つことで、健やかな心と体を育むことができると考えています。園児も職員も一緒に、日本の四季に感謝し、社会の一員であることに喜びを感じて毎日を過ごせるよう心がけています。</p>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_icho01.png" alt="" class="d-season-copyIcon01 u-sp-none">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_icho02.png" alt="" class="d-season-copyIcon02 u-sp-none">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_kaede.png" alt="" class="d-season-copyIcon03 u-sp-none">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_sakura01.png" alt="" class="d-season-copyIcon04 u-sp-none">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_sakura02.png" alt="" class="d-season-copyIcon05 u-sp-none">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_sakura03.png" alt="" class="d-season-copyIcon06 u-sp-none">
  </div><!-- /.d-season-copy -->

  <section class="d-season-login">
    <h2 class="d-season-loginTitle">在園児向け行事レポートのログイン</h2>
    <?php echo do_shortcode('[wpmem_form login]'); ?>
    <?php if (is_user_logged_in()): ?>
      <a href="<?php echo esc_url(home_url('/')); ?>blog/" class="d-season-loginBtn">行事レポートを見る</a>
    <?php endif; ?>
  </section><!-- /.d-season-login -->

  <div class="d-season-icon">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_icho01.png" alt="" class="d-season-copyIcon01 u-pc-none">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_icho02.png" alt="" class="d-season-copyIcon02 u-pc-none">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_kaede.png" alt="" class="d-season-copyIcon03 u-pc-none">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_sakura01.png" alt="" class="d-season-copyIcon04 u-pc-none">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_sakura02.png" alt="" class="d-season-copyIcon05 u-pc-none">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_sakura03.png" alt="" class="d-season-copyIcon06 u-pc-none">
  </div>

  <section class="d-season-calendar">
    <h2 class="d-season-calendarTitle">行事予定</h2>
    <?php echo do_shortcode('[eo_fullcalendar]'); ?>
  </section><!-- /.d-season-login -->

  <section class="d-season-photo">
    <div  class="d-season-photoTitle">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/season/season_text02.png" alt="季節の行事">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_sun.png" alt="" class="d-season-photoIcon01">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_kasa.png" alt="" class="d-season-photoIcon02">
    </div>
    <p class="d-season-photoText">春はひな祭りやこどもの日、夏は水遊びやすいか割り、秋はサンマの食育や秋祭り、冬はすいとん作りや節分など、季節の移り変わりを楽しんでもらえるように、さまざまな行事や食育を行っています。</p>

    <ul class="d-season-photoList" id="season-slider">
      <?php
      if( have_rows('photo_slider') ):
        while ( have_rows('photo_slider') ) : the_row();
        $img_id = '';
        $img_ogj = '';
        $img_src = '';
        $img_id = get_sub_field('photo_img', $post->ID);
        $img_ogj = wp_get_attachment_image_src($img_id, 'slider_md');
        $img_src = $img_ogj[0];
      ?>
        <li class="d-season-photoItem">
          <img src="<?php echo $img_src; ?>" alt="<?php the_sub_field('photo_text'); ?>" class="d-season-photoImg">
          <h3 class="d-season-photoNote"><?php the_sub_field('photo_text'); ?></h3>
        </li>
      <?php
        endwhile;
      endif;
      ?>
    </ul>

    <ul class="d-season-photoNavList" id="season-slider-nav">
      <?php
      if( have_rows('photo_slider') ):
        while ( have_rows('photo_slider') ) : the_row();
        $img_id = '';
        $img_ogj = '';
        $img_src = '';
        $img_id = get_sub_field('photo_img', $post->ID);
        $img_ogj = wp_get_attachment_image_src($img_id, 'slider_md');
        $img_src = $img_ogj[0];
      ?>
        <li class="d-season-photoNavItem">
          <img src="<?php echo $img_src; ?>" alt="<?php the_sub_field('photo_text'); ?>" class="d-season-photoNavImg">
        </li>
      <?php
        endwhile;
      endif;
      ?>
    </ul>
  </section>
</div><!-- /.c-page-content -->

<?php get_footer(); ?>
