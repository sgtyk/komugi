<?php get_header(); ?>
<?php
$date04 = date_create(get_field('pdf04_date', get_page_by_path('pdf')->ID));
$date06 = date_create(get_field('pdf06_date', get_page_by_path('pdf')->ID));
$date07 = date_create(get_field('pdf07_date', get_page_by_path('pdf')->ID));
?>

<div class="c-page-content">
  <div class="c-page-header" style="background-image: url('/komugi/assets/images/main_document.jpg');">
    <h1 class="c-page-headerText">園の詳細資料</h1>
  </div>
  <ul class="c-page-breadList">
    <li class="c-page-bredItem">
      <a href="/komugi/">トップ</a>
    </li>
    <li class="c-page-bredItem">園のこと - 園の詳細資料</li>
  </ul>

  <div class="d-document-copy">
    <p class="d-document-copyText">こむぎ保育園からの各種お知らせを<br class="u-pc-none">PDF にて掲載しております。<br>各リンクからダウンロードをお願いします。</p>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_momidi02.png" alt="" class="d-document-copyIcon01">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_momidi03.png" alt="" class="d-document-copyIcon02">
  </div>

  <section class="d-document-contact">
    <div class="d-document-contactInner">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/document/document_text01.png" alt="入園を考えている方へ" class="d-document-contactTitle">
      <p class="d-document-contactText">入園のお申し込みは小金井市役所保育課へ。<br>見学はお電話で当園に<br class="u-pc-none">お気軽にお問い合わせください</p>
      <div class="d-document-contactInfo">
        <p class="d-document-contactName">小金井市役所保育課</p>
        <p class="d-document-contactTel">042-383-1111<span class="d-document-contactTelSmall">（代表）</span></p>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_river.png" alt="" class="d-document-contactIcon01">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_kawasemi.png" alt="" class="d-document-contactIcon02">
      </div>
    </div>
    <div class="d-document-pdf">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/document/document_img01.png" alt="" class="d-document-pdfImg fade">
      <div class="d-document-pdfInfo">
        <h3 class="d-document-pdfTitle">書類ダウンロード</h3>
        <ul class="d-document-pdfList">
          <li class="d-document-pdfItem c-pdf">
            <a href="<?php echo get_field('pdf04', get_page_by_path('pdf')->ID); ?>" target="_blank">
              <span class="c-pdf-tag">PDF</span>
              入園のしおり
              <span class="c-pdf-date"><?php echo date_format($date04,'Y.m.d').'更新'; ?></span>
            </a>
          </li>
          <li class="d-document-pdfItem c-pdf">
            <a href="<?php echo get_field('pdf05', get_page_by_path('pdf')->ID); ?>" target="_blank">
              <span class="c-pdf-tag">PDF</span>
              苦情対応
            </a>
          </li>
          <li class="d-document-pdfItem c-pdf">
            <a href="<?php echo get_field('pdf06', get_page_by_path('pdf')->ID); ?>" target="_blank">
              <span class="c-pdf-tag">PDF</span>
              よくある質問
              <span class="c-pdf-date"><?php echo date_format($date06,'Y.m.d').'更新'; ?></span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </section><!-- /.d-document-contact -->

  <section class="d-document-parent">
    <h2 class="d-document-parentTitle">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/document/document_text02.png" alt="在園児の保護者の方へ" class="d-document-parentTitleImg">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_yama.png" alt="" class="d-document-parentIcon01">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_cycle.png" alt="" class="d-document-parentIcon02">
    </h2>
    <div class="d-document-pdf">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/document/document_img02.png" alt="" class="d-document-pdfImg fade">
      <div class="d-document-pdfInfo">
        <h3 class="d-document-pdfTitle">書類ダウンロード</h3>
        <ul class="d-document-pdfList">
          <li class="d-document-pdfItem c-pdf">
            <a href="<?php echo get_field('pdf01', get_page_by_path('pdf')->ID); ?>" target="_blank">
              <span class="c-pdf-tag">PDF</span>
              登園許可証
            </a>
          </li>
          <li class="d-document-pdfItem c-pdf">
            <a href="<?php echo get_field('pdf02', get_page_by_path('pdf')->ID); ?>" target="_blank">
              <span class="c-pdf-tag">PDF</span>
              与薬連絡票・与薬指示書
            </a>
          </li>
          <li class="d-document-pdfItem is-large c-pdf">
            <a href="<?php echo get_field('pdf07', get_page_by_path('pdf')->ID); ?>" target="_blank">
              <span class="c-pdf-tag">PDF</span>
              保育園でよくみられる感染症<br class="u-pc-none">一覧
              <span class="c-pdf-date"><?php echo date_format($date07,'Y.m.d').'更新'; ?></span>
            </a>
          </li>
          <li class="d-document-pdfItem c-pdf">
            <a href="<?php echo get_field('pdf10', get_page_by_path('pdf')->ID); ?>" target="_blank">
              <span class="c-pdf-tag">PDF</span>
              就労証明書
            </a>
          </li>
        </ul>
      </div>
    </div>
  </section><!-- /.d-document-parent -->

  <section class="d-document-hospital">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/document/document_text03.png" alt="入園を考えている方へ" class="d-document-hospitalTitle">
    <div class="d-document-hospitalInfo">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/document/document_img03.png" alt="" class="d-document-hospitalImg fade">
      <ul class="d-document-hospitalList">
        <li class="d-document-hospitalItem">
          <span class="d-document-hospitalName">久滋医院</span>
          <a href="http://kuji-clinic.com" target="_blank" class="d-document-hospitalAnchor">http://kuji-clinic.com</a>
        </li>
        <li class="d-document-hospitalItem">
          <span class="d-document-hospitalName">湯山歯科医院</span>
          <a href="http://yuyama-shikaiin.com" target="_blank" class="d-document-hospitalAnchor">http://yuyama-shikaiin.com</a>
        </li>
      </ul>
    </div>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_bus01.png" alt="" class="d-document-hospitalIcon01">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_bus02.png" alt="" class="d-document-hospitalIcon02">
  </section><!-- /.d-document-hospital -->
</div><!-- /.c-page-content -->

<?php get_footer(); ?>
