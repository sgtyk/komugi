<?php

/************************************************************************************************************************************************************************************************************************************************************************************************
 * -- Init
 ************************************************************************************************************************************************************************************************************************************************************************************************/


/**
 * 初期化.
 */
function init_mh() {
  add_theme_support('post-thumbnails');
}

add_action('init', 'init_mh');


/**
 * スタイルシートの読み込み.
 */
function load_kmg_stylesheets() {
  wp_enqueue_style('shared', '/assets/css/index.css');
  wp_enqueue_style('cust', '/assets/css/cust.css');
}

add_action('wp_enqueue_scripts', 'load_kmg_stylesheets');


/**
 * JSファイルの読み込み.
 */
function load_kmg_scripts() {
  // wp_deregister_script('jquery');

  wp_enqueue_script('vendor.dll', '/assets/js/vendor.dll.js');
  wp_enqueue_script('shared', '/assets/js/index.js');
}

add_action('wp_enqueue_scripts', 'load_kmg_scripts');



/**
 * カスタム投稿タイプを追加
 */
function works_kmg_custom_post_construction() {
  $labels = array(
    'name' => 'お知らせ'
  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'menu_position' => 5,
    'has_archive' => true,
    'supports' => array(
      'title',
      'editor',
      'revisions'
    )
  );
  register_post_type('other', $args);
}
add_action('init', 'works_kmg_custom_post_construction');

register_taxonomy(
  'other-category',
  'other',
  array(
    'label' => 'カテゴリー',
    'public' => true,
  )
);


 /**
 * カスタム画像サイズを追加.
 */
function add_kmg_custom_img_size() {
  set_post_thumbnail_size(100, 100, true);

  add_image_size('blog', 610, 455, true);
  add_image_size('slider_md', 1232, 800, true);
  add_image_size('slider_sm', 400, 616, true);
}

add_action('after_setup_theme', 'add_kmg_custom_img_size');


/**
* 概要（抜粋）の省略文字.
*/
function change_kmg_excerpt_more($more) {
  return '...';
}
add_filter('excerpt_more', 'change_kmg_excerpt_more');

/**
* 概要（抜粋）の省略文字.
*/
function modify_kmg_read_more_link() {
    return '<a class="d-blog-postLink" href="' . get_permalink() . '">詳しく見る</a>';
}
add_filter( 'the_content_more_link', 'modify_kmg_read_more_link' );
