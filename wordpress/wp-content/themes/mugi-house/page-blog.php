<?php get_header(); ?>

<div class="c-page-content c-page-content--blog">
  <div class="c-page-header" style="background-image: url('/komugi/assets/images/main_season.jpg');">
    <h1 class="c-page-headerText">季節のたのしみ</h1>
  </div>
  <ul class="c-page-breadList">
    <li class="c-page-bredItem">
      <a href="/komugi/">トップ</a>
    </li>
    <li class="c-page-bredItem">園のこと - 季節のたのしみ</li>
  </ul>

  <section class="d-blog-content">
    <h2 class="d-blog-title">
      行事レポート
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_leaf03.png" alt="" class="d-blog-titleIcon01">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_donguri01.png" alt="" class="d-blog-titleIcon02">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_mugi02.png" alt="" class="d-blog-titleIcon03">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_leaf04.png" alt="" class="d-blog-titleIcon04">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_donguri02.png" alt="" class="d-blog-titleIcon05">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_mugi03.png" alt="" class="d-blog-titleIcon06">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_leaf05.png" alt="" class="d-blog-titleIcon07">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_leaf06.png" alt="" class="d-blog-titleIcon08">
    </h2>

    <ol class="d-blog-postList">
      <?php
      if (is_user_logged_in()):
        $paged = get_query_var('paged');

        $args = array(
          'paged' => $paged,
          'posts_per_page' => 5,
        );
        $news_posts = new WP_Query($args);
        while  ($news_posts->have_posts()) : $news_posts->the_post();
        ?>
        <li class="d-blog-postItem">
          <div class="d-blog-postHeader">
            <h3 class="d-blog-postTitle"><?php the_title(); ?></h3>
            <time class="d-blog-postDate"><?php the_date('Y/m/d'); ?></time>
          </div>
          <div class="d-blog-postLine"></div>
          <div class="d-blog-postContent">
            <?php
            if( have_rows('post_content') ):
              while ( have_rows('post_content') ) : the_row();
              $img_id = '';
              $img_ogj = '';
              $img_src = '';
              $img_id = get_sub_field('post_img', $post->ID);
              $img_ogj = wp_get_attachment_image_src($img_id, 'blog');
              $img_src = $img_ogj[0];
            ?>
              <div class="d-blog-postBlock">
                <img src="<?php echo $img_src; ?>" alt="" class="d-blog-postImg fade">
                <div class="d-blog-postText">
                  <?php the_sub_field('post_text'); ?>
                </div>
              </div>
            <?php
                break;
              endwhile;
            endif;
            ?>
            <p><a class="d-blog-postLink" href="<?php the_permalink(); ?>">詳しく見る</a></p>
          </div>
        </li>
        <?php
        endwhile;
        ?>
      <?php else: ?>
        <?php echo do_shortcode('[wpmem_form login]'); ?>
      <?php endif; ?>
    </ol>
    <?php
    if(function_exists('wp_pagenavi')):
      wp_pagenavi(array('query'=>$news_posts));
    endif;
    wp_reset_postdata();
    ?>
  </section><!-- /.d-blog-content -->
</div><!-- /.c-page-content -->

<?php get_footer(); ?>
