<?php get_header(); ?>

<div class="c-page-content">
  <div class="c-page-header" style="background-image: url('/komugi/assets/images/main_recruit.jpg');">
    <h1 class="c-page-headerText">採用情報</h1>
  </div>
  <ul class="c-page-breadList">
    <li class="c-page-bredItem">
      <a href="/komugi/">トップ</a>
    </li>
    <li class="c-page-bredItem">採用情報</li>
  </ul>

  <div class="d-recruit-copy">
    <p class="d-recruit-copyText">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/recruit/recruit_text01.png" alt="子どもたちとのふれあいを大切にできる方、毎日たくさんの笑顔にあふれた楽しい職場で、私たちと一緒に働きませんか？" class="d-recruit-copyTextImg u-sp-none">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/recruit/recruit_text04.png" alt="子どもたちとのふれあいを大切にできる方、毎日たくさんの笑顔にあふれた楽しい職場で、私たちと一緒に働きませんか？" class="d-recruit-copyTextImg u-pc-none">
    </p>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_zukin04.png" alt="" class="d-recruit-copyIcon01">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_zukin05.png" alt="" class="d-recruit-copyIcon02">
  </div>

  <div class="d-recruit-photo">
    <ul class="d-recruit-photoList">
      <li class="d-recruit-photoItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/recruit/recruit_img01.png" alt="" class="d-recruit-photoImg fade">
      </li>
      <li class="d-recruit-photoItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/recruit/recruit_img02.png" alt="" class="d-recruit-photoImg fade">
      </li>
      <li class="d-recruit-photoItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/recruit/recruit_img03.png" alt="" class="d-recruit-photoImg fade">
      </li>
    </ul>
    <div class="d-recruit-photoContent">
      <p class="d-recruit-photoText">ここでは、毎日いろんな公園に出かけるので、他の園に比べてもたくさん歩きます。商店街の方たちとあいさつをしたり、公園のじゃぶじゃぶ池でびしょ濡れになるほど本気で遊んだり、子どもたちと一緒に散歩へ行くたびに新しい発見がたくさんあって楽しいです。（保育士２年目・女性）</p>
      <p class="d-recruit-photoText">大人数が集まる大きな保育園よりも、全員の顔が身近に感じられるような保育園で働きたいと考えていました。また、ここは職員同士の仲も良く、アットホームでとても楽しい職場です。毎日の散歩も、自分の健康のためにもいい運動になっていますし、子どもたちと一緒に、自分も成長させていただいているなと実感する毎日です。（保育士３年目・男性）</p>
    </div>
  </div><!-- /.d-recruit-photo -->

  <div class="d-recruit-mark">
    <p class="d-recruit-markText">高齢・児童・障害分野の事業所の「働きやすさ」に関する情報を広く公表し、求職者が自分にあった職場を探す支援をするため、東京都が2017 年度に開始した事業です。この事業に当園も参加しています。</p>
  </div><!-- /.d-recruit-mark -->

  <section class="d-recruit-about">
    <h2 class="d-recruit-aboutTitle">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/recruit/recruit_text02.png" alt="採用情報">
    </h2>
    <?php if ( get_field('recruit_message')): ?>
      <p class="d-recruit-aboutStop"><?php echo get_field('recruit_message'); ?></p>
    <?php endif; ?>
    <div class="d-recruit-aboutContent">
      <?php
      if( have_rows('recruit_item') ):
        while ( have_rows('recruit_item') ) : the_row();
      ?>
      <dl class="d-recruit-aboutList">
        <dd class="d-recruit-aboutName"><?php the_sub_field('recruit_item_label'); ?></dd>
        <dt class="d-recruit-aboutText"><?php the_sub_field('recruit_item_text'); ?></dt>
      </dl>
      <?php
        endwhile;
      endif;
      ?>
    </div>
  </section><!-- /.d-recruit-about -->

  <section class="d-recruit-detail">
    <h2 class="d-recruit-detailTitle">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/recruit/recruit_text03.png" alt="募集要項">
    </h2>
    <div class="d-recruit-detailContent">
      <?php
      if( have_rows('recruit_item2') ):
        while ( have_rows('recruit_item2') ) : the_row();
      ?>
        <dl class="d-recruit-aboutList <?php the_sub_field('recruit_item2_row'); ?>">
          <dd class="d-recruit-aboutName"><?php the_sub_field('recruit_item2_label'); ?></dd>
          <dt class="d-recruit-aboutText"><?php the_sub_field('recruit_item2_text'); ?></dt>
        </dl>
      <?php
        endwhile;
      endif;
      ?>
    </div>
  </section><!-- /.d-recruit-detail -->

  <p class="d-recruit-note">まずは気軽にお電話またはe-mailにて<br class="u-pc-none">お問い合わせください。</p>

  <div class="d-recruit-contact">
    <ul class="d-recruit-contactList">
      <li class="d-recruit-contactItem d-recruit-contactItem--tel">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/recruit/recruit_tel.png" alt="採用情報" class="d-recruit-contactTelIcon">
        <div class="d-recruit-contactTel">
          <p class="d-recruit-contactNumber">042-381-1589</p>
          <p class="d-recruit-contactNote">受付時間：平日9：00〜17：00</p>
        </div>
      </li>
      <li class="d-recruit-contactItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/recruit/recruit_mail.png" alt="採用情報" class="d-recruit-contactMailIcon">
        <a href="mailto:komugi@komugi.info" class="d-recruit-contactMail">komugi@komugi.info</a>
      </li>
    </ul>
  </div><!-- /.recruit-contact -->
</div><!-- /.c-page-content -->

<?php get_footer(); ?>
