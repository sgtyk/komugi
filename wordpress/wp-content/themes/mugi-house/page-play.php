<?php get_header(); ?>

<div class="c-page-content">
  <div class="c-page-header" style="background-image: url('/komugi/assets/images/main_play.jpg');">
    <h1 class="c-page-headerText">あそぶ・たべる・いきる</h1>
  </div>
  <ul class="c-page-breadList">
    <li class="c-page-bredItem">
      <a href="/komugi/">トップ</a>
    </li>
    <li class="c-page-bredItem">園のこと - あそぶ・たべる・いきる</li>
  </ul>

  <div class="d-play-copy">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/play/play_text01.png" alt="たくさんのあそびや生活を通して、思いやりを育て、健康な体をつくります。" class="d-play-copyTitle u-sp-none">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/play/play_text03.png" alt="たくさんのあそびや生活を通して、思いやりを育て、健康な体をつくります。" class="d-play-copyTitle u-pc-none">
    <p class="d-play-copyText">こむぎ保育園では、おさんぽを中心とした外遊びや、作物を育てるなどの食育、小学校や地域とも連携した、個性を伸ばす教育などを通して、芯の強い子どもをはぐくんでいきたいと考えています。</p>
    <div class="d-play-copyIconWrap">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_boy01.png" alt="" class="d-play-copyIcon01">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_frog.png" alt="" class="d-play-copyIcon02">
    </div>
  </div>

  <section class="d-play-walk">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/play/play_img01.png" alt="おさんぽ" class="d-play-walkImg fade">
    <div class="d-play-walkInfo">
      <h2 class="d-play-walkTitle">おさんぽ</h2>
      <p class="d-play-walkText">雨の日でもカッパを着て、散歩に出かけます！<br>昆虫や花を見つけたり、地域の人とあいさつを交わしたり。<br>歩くことでたくさんの出会いがあり、風邪を引かない健康な身体をつくることができます。</p>
    </div>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_boy02.png" alt="" class="d-play-walkIcon">
  </section>

  <div class="d-play-map">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/play/play_text02.png" alt="生きることの意味を自分でみつけられる心を。自分の五感を使って、正しいものを選び取る力を。天に向かって伸びる麦の穂のような、凛とした強さを。" class="d-play-mapTitle">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/play/play_map.png" alt="" class="d-play-mapImg">
  </div>

  <ul class="d-play-photoList">
    <li class="d-play-photoItem">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/play/play_img02.png" alt="そとあそび" class="d-play-photoImg fade">
      <h3 class="d-play-photoTitle">そとあそび</h3>
      <p class="d-play-photoText">自然に親しみが持てるように、園庭の遊具もひのきなどの自然素材を使っています。土や緑にもたくさん触れながら、泥んこになって元気いっぱい遊びます！ 木登りやかけっこなど、自分の身体を使って、自由に遊びを考えていきます。</p>
    </li>
    <li class="d-play-photoItem">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/play/play_img03.png" alt="給食" class="d-play-photoImg fade">
      <h3 class="d-play-photoTitle">給食</h3>
      <p class="d-play-photoText">季節の移ろいや習わしを学びながら、旬の食材を使って、和食を中心に献立を考えています。毎日の食事の他にも、とうもろこしの皮むき体験、味噌やうどん作りなどの食育活動を通して、食の成り立ちを学び、自然の恵みに感謝する豊かな心を育てます。</p>
    </li>
    <li class="d-play-photoItem">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/play/play_img04.png" alt="育てる" class="d-play-photoImg fade">
      <h3 class="d-play-photoTitle">育てる</h3>
      <p class="d-play-photoText">「昨日よりも大きくなってる！」「お水をたくさん飲んだからかな？」など、植物や野菜を育てたり観察したりすることで、季節の訪れや思いやりのある心、いのちの大切さを学びます。</p>
    </li>
    <li class="d-play-photoItem">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/play/play_img05.png" alt="伝統あそび" class="d-play-photoImg fade">
      <h3 class="d-play-photoTitle">伝統あそび</h3>
      <p class="d-play-photoText">わらべうたや太鼓などの伝統あそびの時間を、月に一度、専門の先生を招いて行っています。日本に古くから伝わる音色や歌は、音感やリズム感を育み、友だちと一緒に音を合わせる楽しさや体をつくる基礎となっています。</p>
    </li>
    <li class="d-play-photoItem">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/play/play_img06.png" alt="たてわり保育" class="d-play-photoImg fade">
      <h3 class="d-play-photoTitle">たてわり保育</h3>
      <p class="d-play-photoText">さくら組とけやき組は、３～５歳児が一緒に生活するたてわり保育となっています。異年齢がともに過ごすことにより、年長のお友達へのあこがれや、小さい子に対するやさしさやいたわりなど、年齢別保育にはない成長がみられます。</p>
    </li>
    <li class="d-play-photoItem">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/play/play_img07.png" alt="高尾山遠足" class="d-play-photoImg fade">
      <h3 class="d-play-photoTitle">高尾山遠足</h3>
      <p class="d-play-photoText">丈夫なからだをつくってきた園児たちは５歳児になると、卒園遠足で高尾山のふもとから山頂まで登ります。みんなで歩き続ける持久力やはげましあって登る達成感など、たくさんのことが学べる機会となります。</p>
    </li>
  </ul>
</div><!-- /.c-page-content -->

<?php get_footer(); ?>
