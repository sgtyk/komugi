<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="apple-touch-icon" sizes="180x180" href="/komugi/assets/images/ogp/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/komugi/assets/images/ogp/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/komugi/assets/images/ogp/favicon-16x16.png">
  <link rel="manifest" href="/komugi/assets/images/ogp/site.webmanifest">
  <link rel="mask-icon" href="/komugi/assets/images/ogp/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">
  <link rel="stylesheet" href="/komugi/assets/css/index.css">
  <script src="/komugi/assets/js/vendor.dll.js"></script>
  <script src="/komugi/assets/js/index.js"></script>
  <title>こむぎ保育園</title>
  <?php wp_head(); ?>
</head>
<body class="<?php if(is_home()) {echo 'd-top';} ?>">
<header class="c-header">
  <div class="c-header-info">
    <p class="c-header-infoAddress">東京都小金井市東町 4-21-8</p>
    <div class="c-header-infoTel">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_tel.png" alt="tel" class="c-header-infoTelImg">
      042-381-1589
    </div>
  </div>
  <a href="/komugi/" class="c-header-logoAnchor">
    <?php if (is_home()): ?>
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo_top.png" alt="こむぎ保育園" class="c-header-logo">
    <?php else: ?>
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo04.png" alt="こむぎ保育園" class="c-header-logo c-header-logo--under">
    <?php endif; ?>
  </a>
  <nav class="c-nav">
    <ul class="c-nav-list">
      <li class="c-nav-item">
        <a href="/komugi/">トップ</a>
      </li>
      <li class="c-nav-item c-nav-item--about">
        園のこと
        <ul class="c-nav-aboutList">
          <li class="c-nav-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>policy/">大事にしていること</a>
          </li>
          <li class="c-nav-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>play/">あそぶ・たべる・いきる</a>
          </li>
          <li class="c-nav-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>facility/">施設紹介</a>
          </li>
          <li class="c-nav-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>season/">季節のたのしみ</a>
          </li>
          <li class="c-nav-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>document/">園の詳細資料</a>
          </li>
        </ul>
      </li>
      <li class="c-nav-item">
        <a href="<?php echo esc_url(home_url('/')); ?>support/">一時保育・子育て支援</a>
      </li>
      <li class="c-nav-item">
        <a href="<?php echo esc_url(home_url('/')); ?>area/">地域との関わり</a>
      </li>
      <li class="c-nav-item">
        <a href="<?php echo esc_url(home_url('/')); ?>recruit/">採用情報</a>
      </li>
    </ul>
  </nav>
</header>

<?php if (is_home()): ?>
  <header class="c-headerSp c-headerSp--top js-headerSpTop">
    <div class="c-headerSp-inner">
      <a href="/komugi/" class="c-headerSp-logoTopAnchor">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo_top_sp.png" alt="こむぎ保育園" class="c-headerSp-logoTop">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo05.png" alt="こむぎ保育園" class="c-headerSp-logo">
      </a>
      <div class="c-headerSp-open js-headerSp-open">
        <div class="c-headerSp-openIcon">
          <span class="c-headerSp-openIconBar"></span>
          <span class="c-headerSp-openIconBar"></span>
          <span class="c-headerSp-openIconBar"></span>
        </div>
        <p class="c-headerSp-openText">メニュー</p>
      </div>
      <div class="c-headerSp-close js-headerSp-close">
        <div class="c-headerSp-closeIcon"></div>
        <p class="c-headerSp-closeText">閉じる</p>
      </div>
    </div>
  </header>
<?php endif; ?>

<header class="c-headerSp <?php if(is_home()) {echo 'c-headerSp--common js-headerSp';} ?>">
  <div class="c-headerSp-inner">
    <a href="/komugi/">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo05.png" alt="こむぎ保育園" class="c-headerSp-logo">
    </a>
    <div class="c-headerSp-open js-headerSp-open">
      <div class="c-headerSp-openIcon">
        <span class="c-headerSp-openIconBar"></span>
        <span class="c-headerSp-openIconBar"></span>
        <span class="c-headerSp-openIconBar"></span>
      </div>
      <p class="c-headerSp-openText">メニュー</p>
    </div>
    <div class="c-headerSp-close js-headerSp-close">
      <div class="c-headerSp-closeIcon"></div>
      <p class="c-headerSp-closeText">閉じる</p>
    </div>
  </div>
</header>

<nav class="c-navSp">
  <div class="c-navSp-inner">
    <ul class="c-navSp-list">
      <li class="c-navSp-item">
        <a href="/komugi/">
          トップ
          <span class="c-navSp-itemIcon"></span>
        </a>
      </li>
      <li class="c-navSp-item c-navSp-item--about">
        園のこと
        <span class="c-navSp-itemIcon"></span>
        <ul class="c-navSp-aboutList">
          <li class="c-navSp-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>policy/">大事にしていること</a>
          </li>
          <li class="c-navSp-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>play/">あそぶ・たべる・いきる</a>
          </li>
          <li class="c-navSp-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>facility/">施設紹介</a>
          </li>
          <li class="c-navSp-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>season/">季節のたのしみ</a>
          </li>
          <li class="c-navSp-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>document/">園の詳細資料</a>
          </li>
        </ul>
      </li>
      <li class="c-navSp-item">
        <a href="<?php echo esc_url(home_url('/')); ?>support/">
          一時保育・子育て支援
          <span class="c-navSp-itemIcon"></span>
        </a>
      </li>
      <li class="c-navSp-item">
        <a href="<?php echo esc_url(home_url('/')); ?>area/">
          地域との関わり
          <span class="c-navSp-itemIcon"></span>
        </a>
      </li>
      <li class="c-navSp-item">
        <a href="<?php echo esc_url(home_url('/')); ?>recruit/">
          採用情報
          <span class="c-navSp-itemIcon"></span>
        </a>
      </li>
    </ul>
    <div class="c-navSp-pdf">
      <p class="c-navSp-pdfTitle">申請書ダウンロード</p>
      <ul class="c-navSp-pdfList">
        <li class="c-navSp-pdfItem c-pdf">
          <a href="<?php echo get_field('pdf01', get_page_by_path('pdf')->ID); ?>" target="_blank">
            <span class="c-pdf-tag">PDF</span>
            登園許可証
          </a>
        </li>
        <li class="c-navSp-pdfItem c-pdf">
          <a href="<?php echo get_field('pdf02', get_page_by_path('pdf')->ID); ?>" target="_blank">
            <span class="c-pdf-tag">PDF</span>
            与薬連絡表・与薬指示書
          </a>
        </li>
      </ul>
    </div>
    <ul class="c-navSp-contactList">
      <li class="c-navSp-contactItem c-navSp-contactItem--tel">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/recruit/recruit_tel.png" alt="採用情報" class="c-navSp-contactTelIcon">
        <div class="c-navSp-contactTel">
          <p class="c-navSp-contactNumber"><a href="tel:042-381-1589">042-381-1589</a></p>
          <p class="c-navSp-contactNote">受付時間：平日9：00〜17：00</p>
        </div>
      </li>
      <li class="c-navSp-contactItem">
        <div class="c-navSp-contactMailIconWrap">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/recruit/recruit_mail.png" alt="採用情報" class="c-navSp-contactMailIcon">
        </div>
        <a href="mailto:komugi@komugi.info" class="c-navSp-contactMail">komugi@<br>komugi.info</a>
      </li>
    </ul>
  </div>
  <div class="c-navSp-close js-headerSp-close">
    <div class="c-navSp-closeIcon"></div>
    <p class="c-navSp-closeText">閉じる</p>
  </div>
</nav>
