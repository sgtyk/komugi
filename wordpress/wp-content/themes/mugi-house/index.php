<?php get_header(); ?>

<div class="d-top-sliderWrap">
  <div class="d-top-sliderText">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/slider_text02.png" alt="子どもをつくる" class="d-top-sliderTextImg01">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/slider_text03.png" alt="へこたれない" class="d-top-sliderTextImg02">
  </div>
  <div class="d-top-slider js-top-slider">
    <div class="d-top-sliderItem" style="background-image: url('/komugi/assets/images/top/slider_img01.jpg');"></div>
    <div class="d-top-sliderItem" style="background-image: url('/komugi/assets/images/top/slider_img02.jpg');"></div>

    <div class="d-top-sliderItem" style="background-image: url('/komugi/assets/images/top/slider_img03.jpg');"></div>

    <div class="d-top-sliderItem" style="background-image: url('/komugi/assets/images/top/slider_img04.jpg');"></div>

  </div>
</div>

<section class="d-top-news">
  <h2 class="d-top-newsTitle"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/text_news.png" alt="新着情報とお知らせ"></h2>
  <ol class="d-top-newsList">
    <?php
    $args = array(
      'post_type' => array(
        'post',
        'other'
      ),
      'posts_per_page' => 9,
    );
    $news_posts = get_posts($args);
    foreach ($news_posts as $post) : setup_postdata($post);
      if ($post->post_type == 'other') {
        $category = get_the_terms( $post->ID, 'other-category' );
        $cat_name = $category[0]->name;
        $cat_slug = $category[0]->slug;
      } else {
        $cat_name = 'ブログ';
        $cat_slug = 'blog';
      }
    ?>
      <li class="d-top-newsItem">
        <?php if ($cat_name == 'blog'): ?>
          <a href="<?php the_permalink(); ?>" class="d-top-newsAnchor">
        <?php else: ?>
          <a href="<?php the_permalink(); ?>" class="d-top-newsAnchor">
        <?php endif; ?>
          <date class="d-top-newsDate"><?php echo get_the_date('Y-m-d', $post->ID); ?></date>
          <span class="d-top-newsTag d-top-newsTag--<?php echo $cat_slug; ?>"><?php echo $cat_name; ?></span>
          <p class="d-top-newsText"><?php the_title(); ?></p>
        </a>
      </li>
    <?php
    endforeach;
    wp_reset_postdata();
    ?>
  </ol><!-- /.d-top-newsList. -->
</section><!-- /.d-top-news -->

<div class="d-top-flower">
  <section class="d-top-about">
    <h2 class="d-top-aboutTitle"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/text_about.png" alt="園のこと"></h2>
    <ul class="d-top-aboutList">
      <li class="d-top-aboutItem fade">
        <a href="<?php echo esc_url(home_url('/')); ?>policy/" class="d-top-aboutAnchor">
          <div class="d-top-aboutImgWrap">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/img_top01.png" alt="" class="d-top-aboutImg">
            <span class="d-top-aboutImgIcon"></span>
          </div>
          <p class="d-top-aboutText">大事にしていること</p>
        </a>
      </li>
      <li class="d-top-aboutItem fade">
        <a href="<?php echo esc_url(home_url('/')); ?>play/" class="d-top-aboutAnchor">
          <div class="d-top-aboutImgWrap">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/img_top02.png" alt="" class="d-top-aboutImg">
            <span class="d-top-aboutImgIcon"></span>
          </div>
          <p class="d-top-aboutText">あそぶ・たべる・いきる</p>
        </a>
      </li>
      <li class="d-top-aboutItem fade">
        <a href="<?php echo esc_url(home_url('/')); ?>facility/" class="d-top-aboutAnchor">
          <div class="d-top-aboutImgWrap">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/img_top03.png" alt="" class="d-top-aboutImg">
            <span class="d-top-aboutImgIcon"></span>
          </div>
          <p class="d-top-aboutText">施設紹介</p>
        </a>
      </li>
      <li class="d-top-aboutItem fade">
        <a href="<?php echo esc_url(home_url('/')); ?>season/" class="d-top-aboutAnchor">
          <div class="d-top-aboutImgWrap">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/img_top04.png" alt="" class="d-top-aboutImg">
            <span class="d-top-aboutImgIcon"></span>
          </div>
          <p class="d-top-aboutText">季節のたのしみ</p>
        </a>
      </li>
      <li class="d-top-aboutItem fade">
        <a href="<?php echo esc_url(home_url('/')); ?>document/" class="d-top-aboutAnchor">
          <div class="d-top-aboutImgWrap">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/img_top05.png" alt="" class="d-top-aboutImg">
            <span class="d-top-aboutImgIcon"></span>
          </div>
          <p class="d-top-aboutText">園の詳細資料</p>
        </a>
      </li>
    </ul>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_hand01.png" alt="" class="d-top-aboutIcon01">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_hand02.png" alt="" class="d-top-aboutIcon02">
  </section>

  <div class="d-top-policy fade">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/text_policy.png" alt="" class="d-top-policyTextImg">
      <ol class="d-top-policyList">
        <li class="d-top-policyItem">
          <p class="d-top-policyItemInner">本物を五感で感じ、<br>本物を選びとる力をつける保育</p>
        </li>
        <li class="d-top-policyItem">
          <p class="d-top-policyItemInner">造形や絵画、観劇、音楽など、<br>また伝統文化を大切にする保育</p>
        </li>
        <li class="d-top-policyItem">
          <p class="d-top-policyItemInner">個性豊かな人間を育む保育</p>
        </li>
      </ol>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_bird02.png" alt="" class="d-top-policyIcon01">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_zukin02.png" alt="" class="d-top-policyIcon02">
  </div><!-- /.d-top-policy -->
</div><!-- /.d-top-flower -->

<div class="d-top-other">
  <ul class="d-top-otherList fade">
    <li class="d-top-otherItem">
      <a href="<?php echo esc_url(home_url('/')); ?>support/" class="d-top-otherAnchor">
        <div class="d-top-otherImgWrap">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/img_top06.png" alt="" class="d-top-otherImg">
          <span class="d-top-otherImgIcon"></span>
        </div>
        <p class="d-top-otherText"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/text_support.png" alt="一時保育・子育て支援"></p>
        <p class="d-top-otherNote">当園の一時預かり保育や子育て支援について</p>
      </a>
    </li>
    <li class="d-top-otherItem">
      <a href="<?php echo esc_url(home_url('/')); ?>area/" class="d-top-otherAnchor">
        <div class="d-top-otherImgWrap">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/img_top07.png" alt="" class="d-top-otherImg">
          <span class="d-top-otherImgIcon"></span>
        </div>
        <p class="d-top-otherText"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/text_area.png" alt="地域との関わり"></p>
        <p class="d-top-otherNote">園は園児ともに、地域の一員として社会と関わります</p>
      </a>
    </li>
    <li class="d-top-otherItem u-pc-none">
      <a href="<?php echo esc_url(home_url('/')); ?>recruit/" class="d-top-otherAnchor">
        <div class="d-top-otherImgWrap">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/img_top08.png" alt="" class="d-top-otherImg">
          <span class="d-top-otherImgIcon"></span>
        </div>
        <p class="d-top-otherText"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/text_recruit.png" alt="採用情報"></p>
        <p class="d-top-otherNote">園児たちの笑顔に寄り添う職員の募集です</p>
      </a>
    </li>
  </ul>
  <div class="d-top-recruit fade u-sp-none">
    <a href="<?php echo esc_url(home_url('/')); ?>recruit/" class="d-top-recruitAnchor">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/img_top08.png" alt="" class="d-top-recruitImg">
      <div class="d-top-recruitInfo">
        <h3 class="d-top-recruitTitle"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/text_recruit.png" alt="採用情報"></h3>
        <p class="d-top-recruitText">園児たちの笑顔に寄り添う職員の募集です</p>
        <span class="d-top-recruitBtn"></span>
      </div>
    </a>
  </div>
</div>

<?php get_footer(); ?>
