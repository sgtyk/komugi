<?php if (is_home()): ?>
  <footer class="c-footer">
    <a href="#" class="c-footer-topBack js-footer-topBack">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top_back_img.png" alt="" class="c-footer-topBackImg">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top_back_text.png" alt="" class="c-footer-topBackText">
    </a>
    <nav class="c-footer-nav">
      <ul class="c-footer-navList">
        <li class="c-footer-navItem">
          <a href="/komugi/">トップ</a>
        </li>
        <li class="c-footer-navItem u-sp-none">
          <a href="<?php echo esc_url(home_url('/')); ?>policy/">園のこと</a>
        </li>
        <li class="c-footer-navItem">
          <a href="<?php echo esc_url(home_url('/')); ?>support/">一時保育・子育て支援</a>
        </li>
        <li class="c-footer-navItem">
          <a href="<?php echo esc_url(home_url('/')); ?>area/">地域との関わり</a>
        </li>
        <li class="c-footer-navItem">
          <a href="<?php echo esc_url(home_url('/')); ?>recruit/">採用情報</a>
        </li>
      </ul>
      <ul class="c-footer-subnavList">
        <li class="c-footer-subnavItem">
          <a href="<?php echo esc_url(home_url('/')); ?>policy/">大事にしていること</a>
        </li>
        <li class="c-footer-subnavItem">
          <a href="<?php echo esc_url(home_url('/')); ?>play/">あそぶ・たべる・いきる</a>
        </li>
        <li class="c-footer-subnavItem">
          <a href="<?php echo esc_url(home_url('/')); ?>facility/">施設紹介</a>
        </li>
        <li class="c-footer-subnavItem">
          <a href="<?php echo esc_url(home_url('/')); ?>season/">季節のたのしみ</a>
        </li>
        <li class="c-footer-subnavItem">
          <a href="<?php echo esc_url(home_url('/')); ?>document/">園の詳細資料</a>
        </li>
      </ul>
      <div class="c-footer-about">
        <a href="<?php echo esc_url(home_url('/')); ?>policy/" class="c-footer-aboutTitle">園のこと</a>
        <ul class="c-footer-aboutList">
          <li class="c-footer-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>policy/">大事にしていること</a>
          </li>
          <li class="c-footer-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>play/">あそぶ・たべる・いきる</a>
          </li>
          <li class="c-footer-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>facility/">施設紹介</a>
          </li>
          <li class="c-footer-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>season/">季節のたのしみ</a>
          </li>
          <li class="c-footer-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>document/">園の詳細資料</a>
          </li>
        </ul>
      </div>
    </nav>
    <div class="c-footer-content">
      <div class="c-footer-contentInner">
        <div class="c-footer-info">
          <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top/map_top.png" alt="" class="c-footer-infoMap">
          <div class="c-footer-infoInner">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo02.png" alt="" class="c-footer-infoLogo">
            <address class="c-footer-infoAddress">
              〒184-0011<br>
              東京都小金井市東町 4-21-8
            </address>
            <div class="c-footer-infoMail">
              e-mail : <a href="mailto:komugi@komugi.info">komugi@komugi.info</a>
            </div>
            <p class="c-footer-infoTel">TEL : 042-381-1589</p>
          </div>
        </div>
        <div class="c-footer-link">
          <div class="c-footer-linkInner">
            <a href="http://komugi.info/" target="_blank">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo_fukushi.png" alt="" class="c-footer-linkLogo">
            </a>
            <ul class="c-footer-linkList">
              <li class="c-footer-linkItem">
                <a href="http://komugi.info/" target="_blank">社会福祉法人友好福祉会</a>
              </li>
              <li class="c-footer-linkItem">
                <a href="http://komugi.info/muginoie/" target="_blank">障害者支援施設麦の家</a>
              </li>
            </ul>
          </div>
          <small class="c-footer-linkCopy">copyright@友好福祉会 allright reserved.</small>
        </div>
      </div>
      <div class="c-footer-pdf">
        <ul class="c-footer-pdfList">
          <li class="c-footer-pdfItem c-pdf">
            <a href="<?php echo get_field('pdf01', get_page_by_path('pdf')->ID); ?>" target="_blank">
              <span class="c-pdf-tag">PDF</span>
              登園許可証
            </a>
          </li>
          <li class="c-footer-pdfItem c-pdf">
            <a href="<?php echo get_field('pdf02', get_page_by_path('pdf')->ID); ?>" target="_blank">
              <span class="c-pdf-tag">PDF</span>
              与薬連絡票・与薬指示書
            </a>
          </li>
          <li class="c-footer-pdfItem c-pdf">
            <a href="<?php echo get_field('pdf03', get_page_by_path('pdf')->ID); ?>" target="_blank">
              <span class="c-pdf-tag">PDF</span>
              貸しホールのご案内
            </a>
          </li>
        </ul>
      </div>
    </div><!-- /.c-footer-content -->
  </footer>
<?php else: ?>
  <footer class="c-footer c-footer--under">
    <a href="#" class="c-footer-topBack js-footer-topBack">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top_back_img.png" alt="" class="c-footer-topBackImg">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/top_back_text.png" alt="" class="c-footer-topBackText">
    </a>
    <nav class="c-footer-nav">
      <ul class="c-footer-navList">
        <li class="c-footer-navItem">
          <a href="/komugi/">トップ</a>
        </li>
        <li class="c-footer-navItem u-sp-none">
          <a href="<?php echo esc_url(home_url('/')); ?>policy/">園のこと</a>
        </li>
        <li class="c-footer-navItem">
          <a href="<?php echo esc_url(home_url('/')); ?>support/">一時保育・子育て支援</a>
        </li>
        <li class="c-footer-navItem">
          <a href="<?php echo esc_url(home_url('/')); ?>area/">地域との関わり</a>
        </li>
        <li class="c-footer-navItem">
          <a href="<?php echo esc_url(home_url('/')); ?>recruit/">採用情報</a>
        </li>
      </ul>
      <ul class="c-footer-subnavList">
        <li class="c-footer-subnavItem">
          <a href="<?php echo esc_url(home_url('/')); ?>policy/">大事にしていること</a>
        </li>
        <li class="c-footer-subnavItem">
          <a href="<?php echo esc_url(home_url('/')); ?>play/">あそぶ・たべる・いきる</a>
        </li>
        <li class="c-footer-subnavItem">
          <a href="<?php echo esc_url(home_url('/')); ?>facility/">施設紹介</a>
        </li>
        <li class="c-footer-subnavItem">
          <a href="<?php echo esc_url(home_url('/')); ?>season/">季節のたのしみ</a>
        </li>
        <li class="c-footer-subnavItem">
          <a href="<?php echo esc_url(home_url('/')); ?>document/">園の詳細資料</a>
        </li>
      </ul>
      <div class="c-footer-about">
        <a href="<?php echo esc_url(home_url('/')); ?>policy/" class="c-footer-aboutTitle">園のこと</a>
        <ul class="c-footer-aboutList">
          <li class="c-footer-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>policy/">大事にしていること</a>
          </li>
          <li class="c-footer-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>play/">あそぶ・たべる・いきる</a>
          </li>
          <li class="c-footer-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>facility/">施設紹介</a>
          </li>
          <li class="c-footer-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>season/">季節のたのしみ</a>
          </li>
          <li class="c-footer-aboutItem">
            <a href="<?php echo esc_url(home_url('/')); ?>document/">園の詳細資料</a>
          </li>
        </ul>
      </div>
    </nav>
    <a href="/komugi/">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo03.png" alt="" class="c-footer-logo">
    </a>
    <div class="c-footer-content">
      <div class="c-footer-contentInner">
        <div class="c-footer-link">
          <div class="c-footer-linkInner">
            <a href="http://komugi.info/" target="_blank">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo_fukushi.png" alt="" class="c-footer-linkLogo">
            </a>
            <ul class="c-footer-linkList">
              <li class="c-footer-linkItem">
                <a href="http://komugi.info/" target="_blank">社会福祉法人友好福祉会</a>
              </li>
              <li class="c-footer-linkItem">
                <a href="http://komugi.info/muginoie/" target="_blank">障害者支援施設麦の家</a>
              </li>
            </ul>
          </div>
          <small class="c-footer-linkCopy">copyright@友好福祉会 allright reserved.</small>
        </div>
      </div>
    </div><!-- /.c-footer-content -->
  </footer>
<?php endif; ?>


<?php wp_footer(); ?>
</body>
</html>
