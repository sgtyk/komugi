<?php get_header(); ?>

<div class="c-page-content">
  <div class="c-page-header" style="background-image: url('/komugi/assets/images/main_area.jpg');">
    <h1 class="c-page-headerText">地域との関わり</h1>
  </div>
  <ul class="c-page-breadList">
    <li class="c-page-bredItem">
      <a href="/komugi/">トップ</a>
    </li>
    <li class="c-page-bredItem">地域との関わり</li>
  </ul>

  <div class="d-area-copy">
    <p class="d-area-copyText">園では、地域の方々との交流を積極的に行っています。地域のさまざまな世代の人と交流することで、園児をはじめ職員も、社会の一員であることの自覚につなげ、園と地域とのつながりを大切にしていきたいと考えています。</p>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_leaf07.png" alt="" class="d-area-copyIcon01">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_leaf08.png" alt="" class="d-area-copyIcon02">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_ahiru.png" alt="" class="d-area-copyIcon03">
  </div>

  <div class="d-area-photo">
    <ul class="d-area-photoList">
      <li class="d-area-photoItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/area/area_img01.png" alt="園での交流" class="d-area-photoImg">
        <p class="d-area-photoTitle">園での交流</p>
        <p class="d-area-photoText">保育士体験の受け入れやさくら体操では、地域の小中高生や地元の方たちと園児や保育士が交流し、こむぎ保育園への理解を深めてもらっています。</p>
      </li>
      <li class="d-area-photoItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/area/area_img02.png" alt="老人ホーム『つきみの園』へ訪問" class="d-area-photoImg">
        <p class="d-area-photoTitle">老人ホーム『つきみの園』へ訪問</p>
        <p class="d-area-photoText">４～５歳児が年6 回、『つきみの園』を訪問し、歌の発表をしたり、利用者の方々と交流したりするなど、たのしいひと時をすごします。</p>
      </li>
      <li class="d-area-photoItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/area/area_img03.png" alt="園の職員による活動" class="d-area-photoImg">
        <p class="d-area-photoTitle">園の職員による活動</p>
        <p class="d-area-photoText">毎月1 日、地域住民による新小金井駅前の清掃活動に参加するほか、地域の方々が集まる「お茶の間カフェ」に参加するなど、交流を深めています。</p>
      </li>
      <li class="d-area-photoItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/area/area_img04.png" alt="地域住民に園をご利用いただく活動" class="d-area-photoImg">
        <p class="d-area-photoTitle">地域住民に園をご利用いただく活動</p>
        <p class="d-area-photoText">こむぎ保育園の保育室を貸ホールとして開放し、年に数回、新小金井西口商店会主催の落語会が開催されています。</p>
      </li>
    </ul>
  </div><!-- /.d-area-photo -->

  <section class="d-area-hall">
    <h2 class="d-area-hallTitle">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/area/area_text01.png" alt="貸しホールのご案内">
    </h2>
    <div class="d-area-hallInner">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/area/area_img05.png" alt="貸しホールのご案内" class="d-area-hallImg">
      <div class="d-area-hallInfo">
        <p class="d-area-hallText">保育室を使用していない時間帯、保育室を貸しホールとして無償で地域の方にご利用いただいております。</p>
        <div class="d-area-hallListWrap">
          <dl class="d-area-hallList">
            <dt class="d-area-hallItemTitle">ご利用可能日</dt>
            <dd class="d-area-hallItemText">土日祝日午前9時～午後７時</dd>
          </dl>
        </div>
        <div class="d-area-hallBtn c-pdf">
          <a href="<?php echo get_field('pdf03', get_page_by_path('pdf')->ID); ?>" target="_blank">
            <span class="c-pdf-tag">PDF</span>
            詳細PDFダウンロード
          </a>
        </div>
      </div>
    </div>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_train.png" alt="" class="d-area-hallIcon">
  </section><!-- /.d-area-hall -->
</div><!-- /.c-page-content -->

<?php get_footer(); ?>
