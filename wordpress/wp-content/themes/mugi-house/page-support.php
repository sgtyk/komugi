<?php get_header(); ?>
<?php
$date09 = date_create(get_field('pdf09_date', get_page_by_path('pdf')->ID));
?>

<div class="c-page-content">
  <div class="c-page-header" style="background-image: url('/komugi/assets/images/main_support.jpg');">
    <h1 class="c-page-headerText">一時保育・子育て支援</h1>
  </div>
  <ul class="c-page-breadList">
    <li class="c-page-bredItem">
      <a href="/komugi/">トップ</a>
    </li>
    <li class="c-page-bredItem">一時保育・子育て支援</li>
  </ul>

  <div class="d-support-copy">
    <p class="d-support-copyText">一時預かり保育・子育て支援など、地域のみなさまに貢献できる事業に取り組んでいます。</p>
  </div>

  <section class="d-support-once">
    <h2 class="d-support-onceTitle">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/support/support_text01.png" alt="一時預かり保育" class="d-support-onceTitleImg">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_kuri.png" alt="" class="d-support-onceIcon01">
    </h2>
    <div class="d-support-onceInner">
      <p class="d-support-onceText u-pc-none">預かり保育には、保護者の方が何等かの理由により、緊急かつ一時的に保育を必要とされる場合の「一時預かり保育（余裕活用型）」と、保護者の方が就労のために保育を必要とされる場合の「定期利用保育」の二種類があります。</p>
      <div class="d-support-onceImgWrap">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/support/support_img01.png" alt="" class="d-support-onceImg fade">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/support/support_img02.png" alt="" class="d-support-onceImg fade">
      </div>
      <div class="d-support-onceInfo">
        <p class="d-support-onceText u-sp-none">預かり保育には、保護者の方が何等かの理由により、緊急かつ一時的に保育を必要とされる場合の「一時預かり保育（余裕活用型）」と、保護者の方が就労のために保育を必要とされる場合の「定期利用保育」の二種類があります。</p>
        <div class="d-support-target">
          <p class="d-support-targetTitle">対象の方</p>
          <div class="d-support-targetInfo">
            <ol class="d-support-targetList">
              <li class="d-support-targetItem">
                小金井市在住で、生後６ヶ月から就学前までの健康なお子様（定期利用は１歳児クラス以上）。
              </li>
              <li class="d-support-targetItem">
                保育園に通われていないお子様。
              </li>
            </ol>
            <p class="d-support-targetNote">上記項目、いずれも満たした方のみ、お預かりが可能となっています。</p>
          </div>
        </div>
        <div class="d-support-onceListWrap">
          <dl class="d-support-onceList">
            <dt class="d-support-onceItemTitle">保育時間</dt>
            <dd class="d-support-onceItemText">月～金曜日　午前8時30分～午後4時30分</dd>
          </dl>

          <dl class="d-support-onceList">
            <dt class="d-support-onceItemTitle">お問い合わせ</dt>
            <dd class="d-support-onceItemText">042-381-1589 （午前8時～午後5時）</dd>
          </dl>
        </div>
        <div class="d-support-pdfItem c-pdf">
          <a href="<?php echo get_field('pdf08', get_page_by_path('pdf')->ID); ?>" target="_blank">
            <span class="c-pdf-tag">PDF</span>
            詳細PDFダウンロード
          </a>
        </div>
      </div>
    </div>
  </section><!-- /.d-support-once -->

  <section class="d-support-child">
    <h2 class="d-support-childTitle">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/support/support_text02.png" alt="子育て支援 妊産婦の方へ" class="d-support-childTitleImg">
    </h2>
    <div class="d-support-childInner">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/support/support_img03.png" alt="" class="d-support-childImg fade">
      <div class="d-support-childInfo">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/support/support_text03.png" alt="こむぎ保育園に遊びにいらっしゃいませんか？" class="d-support-childInfoTitle u-sp-none">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/support/support_text04.png" alt="こむぎ保育園に遊びにいらっしゃいませんか？" class="d-support-childInfoTitle u-pc-none">
        <p class="d-support-childInfoText">こむぎ保育園では、子育て支援事業として、「ひよこ学習」と「おやつを食べよう」を開催しています。子育て中の地域の方はどなたでもご参加いただけますので、お気軽にお申し込みください。</p>
      </div>
    </div>
    <div class="d-support-childListWrap">
      <dl class="d-support-childList">
        <dt class="d-support-childItemTitle">ひよこ学習</dt>
        <dd class="d-support-childItemText">保育士や看護師が子育て相談にのり、育児の仕方、ふれあい遊びなどをお伝えします。</dd>
      </dl>
      <dl class="d-support-childList">
        <dt class="d-support-childItemTitle">おやつを食べよう</dt>
        <dd class="d-support-childItemText">保育園に関心のある地域の方に園生活を体験していただく機会です。おやつの試食もあります。</dd>
      </dl>
    </div>
    <div class="d-support-pdfItem is-child is-large c-pdf">
      <a href="<?php echo get_field('pdf09', get_page_by_path('pdf')->ID); ?>" target="_blank">
        <span class="c-pdf-tag">PDF</span>
        詳細PDFダウンロード
      </a>
    </div>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_cat.png" alt="" class="d-support-childIcon01">
  </section><!-- /.d-support-child -->
</div><!-- /.c-page-content -->

<?php get_footer(); ?>
