<?php get_header(); ?>

<div class="c-page-content">
  <div class="c-page-header" style="background-image: url('/komugi/assets/images/main_facility.jpg');">
    <h1 class="c-page-headerText">施設紹介</h1>
  </div>
  <ul class="c-page-breadList">
    <li class="c-page-bredItem">
      <a href="/komugi/">トップ</a>
    </li>
    <li class="c-page-bredItem">園のこと - 施設紹介</li>
  </ul>

  <div class="d-facility-copy">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_text01.png" alt="太陽の光と木のぬくもりを大切にした、ゆとりある空間" class="d-facility-copyTitle u-sp-none">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_text03.png" alt="太陽の光と木のぬくもりを大切にした、ゆとりある空間" class="d-facility-copyTitle u-pc-none">
    <p class="d-facility-copyText">こむぎ保育園では、子どもたちと保育者が安心して関わることができるような、ゆったりとした空間づくりを心がけています。子どもたちが毎日ふれる椅子やテーブル、建物の壁や柱などには自然の素材を使い、優しくあたたかい手ざわりを大切にしています。</p>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_tree01.png" alt="" class="d-facility-copyIcon01">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_tree02.png" alt="" class="d-facility-copyIcon02">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_sun.png" alt="" class="d-facility-copyIcon03">
  </div>

  <div class="d-facility-about">
    <div class="d-facility-aboutInfo">
      <dl class="d-facility-aboutList">
        <dt class="d-facility-aboutTitle">名称</dt>
        <dd class="d-facility-aboutText">社会福祉法人　友好福祉会　こむぎ保育園</dd>
      </dl>
      <dl class="d-facility-aboutList">
        <dt class="d-facility-aboutTitle">種別</dt>
        <dd class="d-facility-aboutText">保育所( 認可保育所)</dd>
      </dl>
      <dl class="d-facility-aboutList">
        <dt class="d-facility-aboutTitle">アクセス</dt>
        <dd class="d-facility-aboutText">JR中央線「東小金井駅」より徒歩8分<br>西武多摩川線「新小金井駅」より徒歩1分</dd>
      </dl>
      <dl class="d-facility-aboutList">
        <dt class="d-facility-aboutTitle">住所</dt>
        <dd class="d-facility-aboutText">東京都小金井市東町 4-21-8</dd>
      </dl>
      <dl class="d-facility-aboutList">
        <dt class="d-facility-aboutTitle">TEL</dt>
        <dd class="d-facility-aboutText">042-381-1589</dd>
      </dl>
      <dl class="d-facility-aboutList">
        <dt class="d-facility-aboutTitle">FAX</dt>
        <dd class="d-facility-aboutText">042-381-1615</dd>
      </dl>
      <dl class="d-facility-aboutList">
        <dt class="d-facility-aboutTitle">email</dt>
        <dd class="d-facility-aboutText">komugi@komugi.info</dd>
      </dl>
      <dl class="d-facility-aboutList">
        <dt class="d-facility-aboutTitle">サイト</dt>
        <dd class="d-facility-aboutText">http://www.komugi.info/</dd>
      </dl>
    </div><!-- /.d-facility-aboutInfo -->
    <div class="d-facility-aboutMap">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_map.png" alt="地図" class="d-facility-aboutMapImg">
      <a href="https://goo.gl/maps/xWmnQSjraPE2" class="d-facility-aboutBtn" target="_blank">Google mapで見る</a>
    </div>
  </div><!-- /.d-facility-about -->

  <div class="d-facility-detail">
    <dl class="d-facility-deitalList">
      <dt class="d-facility-deitalTitle">定員</dt>
      <dd class="d-facility-deitalText">118名</dd>
    </dl>
    <dl class="d-facility-deitalList">
      <dt class="d-facility-deitalTitle">規模</dt>
      <dd class="d-facility-deitalText">敷地面積：334.11㎡ ／ 延床面積：751.2㎡</dd>
    </dl>
    <dl class="d-facility-deitalList">
      <dt class="d-facility-deitalTitle">開所時間</dt>
      <dd class="d-facility-deitalText">午前７時～午後７時</dd>
    </dl>
    <dl class="d-facility-deitalList">
      <dt class="d-facility-deitalTitle">休日</dt>
      <dd class="d-facility-deitalText">日曜日、国民の祝祭日、12月29日～1月３日まで</dd>
    </dl>
    <dl class="d-facility-deitalList">
      <dt class="d-facility-deitalTitle">入所対象児</dt>
      <dd class="d-facility-deitalText">0～５歳　就学前まで<br>※生後57日以降のお子様で、健康診断の結果、集団生活を営むことができる健康な児童</dd>
    </dl>
    <dl class="d-facility-deitalList">
      <dt class="d-facility-deitalTitle">延長保育</dt>
      <dd class="d-facility-deitalText">午後６時～７時<br>定期利用料金：月額3,000円 ／ スポット利用料金：1回1,000円（上限なし）</dd>
    </dl>
    <dl class="d-facility-deitalList">
      <dt class="d-facility-deitalTitle">保育支援</dt>
      <dd class="d-facility-deitalText">常勤看護師の配置があるほか、臨床心理士、言語発達の専門家などの巡回相談があります。</dd>
    </dl>
    <dl class="d-facility-deitalList">
      <dt class="d-facility-deitalTitle">父母<br class="u-pc-none">参加関連</dt>
      <dd class="d-facility-deitalText">年数回、1歳児以上の保護者対象に保育連絡会を開催。<br>プレイデー( 運動会)や夏祭りなどの行事は、保護者も一緒に参加します。</dd>
    </dl>
  </div>

  <ul class="d-facility-photoList">
    <li class="d-facility-photoItem">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_img01.png" alt="" class="fade">
    </li>
    <li class="d-facility-photoItem">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_img02.png" alt="" class="fade">
    </li>
    <li class="d-facility-photoItem">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_img03.png" alt="" class="fade">
    </li>
  </ul>

  <section class="d-facility-introduction">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_text02.png" alt="お部屋・園庭の紹介" class="d-facility-introductionTitle">
    <h2 class="d-facility-introductionTitle">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_bird02.png" alt="" class="d-facility-introductionIcon01 u-sp-none">
      <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_bird03.png" alt="" class="d-facility-introductionIcon02 u-sp-none">
    </h2>
    <p class="d-facility-introductionText">
      当園の施設は、2009 年にこむぎ保育園として開園した「現園」と、道路を挟んだ隣に 2015 年に新しく建設した「新園」、さらに少し離れた場所に617 ㎡の広い園庭が 2017 年に開園しました。
    </p>
    <ul class="d-facility-introductionList">
      <li class="d-facility-introductionItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_img04.png" alt="現園１F / エントランス" class="d-facility-introductionImg fade">
        <h3 class="d-facility-introductionNote">現園１F / エントランス</h3>
      </li>
      <li class="d-facility-introductionItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_img05.png" alt="現園１F / 玄関・ホール" class="d-facility-introductionImg fade">
        <h3 class="d-facility-introductionNote">現園１F / 玄関・ホール</h3>
      </li>
      <li class="d-facility-introductionItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_img06.png" alt="現園１F / 給食室" class="d-facility-introductionImg fade">
        <h3 class="d-facility-introductionNote">現園１F / 給食室</h3>
      </li>
      <li class="d-facility-introductionItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_img07.png" alt="現園３F / ２歳児保育" class="d-facility-introductionImg fade">
        <h3 class="d-facility-introductionNote">現園３F / ２歳児保育室</h3>
      </li>
      <li class="d-facility-introductionItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_img08.png" alt="現園３F ／ ２歳児保育" class="d-facility-introductionImg fade">
        <h3 class="d-facility-introductionNote">現園３F ／ ２歳児保育室</h3>
      </li>
      <li class="d-facility-introductionItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_img09.png" alt="現園２F / １歳児保育室" class="d-facility-introductionImg fade">
        <h3 class="d-facility-introductionNote">現園２F / １歳児保育室</h3>
      </li>
      <li class="d-facility-introductionItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_img10.png" alt="現園２F / １歳児保育室" class="d-facility-introductionImg fade">
        <h3 class="d-facility-introductionNote">現園２F / １歳児保育室</h3>
      </li>
      <li class="d-facility-introductionItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_img11.png" alt="新園３F / ３〜５歳児保育室" class="d-facility-introductionImg fade">
        <h3 class="d-facility-introductionNote">新園３F / ３〜５歳児保育室</h3>
      </li>
      <li class="d-facility-introductionItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_img12.png" alt="新園１F / ０歳児保育室" class="d-facility-introductionImg fade">
        <h3 class="d-facility-introductionNote">新園１F / ０歳児保育室</h3>
      </li>
      <li class="d-facility-introductionItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_img13.png" alt="新園２F / ３〜５歳児保育室" class="d-facility-introductionImg fade">
        <h3 class="d-facility-introductionNote">新園２F / ３〜５歳児保育室</h3>
      </li>
      <li class="d-facility-introductionItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_img14.png" alt="園庭" class="d-facility-introductionImg fade">
        <h3 class="d-facility-introductionNote">園庭</h3>
      </li>
      <li class="d-facility-introductionItem">
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facility/facility_img15.png" alt="遊具" class="d-facility-introductionImg fade">
        <h3 class="d-facility-introductionNote">遊具</h3>
      </li>
    </ul>
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_bird02.png" alt="" class="d-facility-introductionIcon01 u-pc-none">
    <img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/icon/icon_bird03.png" alt="" class="d-facility-introductionIcon02 u-pc-none">
  </section><!-- /.d-facility-introduction -->
</div><!-- /.c-page-content -->

<?php get_footer(); ?>
